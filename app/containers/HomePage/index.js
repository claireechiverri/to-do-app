/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectReducer from 'utils/injectReducer';
import { makeSelectLoading, makeSelectError } from 'containers/App/selectors';
import Button from 'components/Button';
import H2 from 'components/H2';
import Form from './Form';
import Input from './Input';
import Section from './Section';
import messages from './messages';
import { changeTask, addNewTask } from './actions';
import { makeSelectTask } from './selectors';
import reducer from './reducer';

/* eslint-disable react/prefer-stateless-function */
export class HomePage extends React.PureComponent {
  render() {
    return (
      <article>
        <Helmet>
          <title>Home Page</title>
          <meta
            name="description"
            content="A React.js Boilerplate application homepage"
          />
        </Helmet>
        <div>
          <Section>
            <H2>
              <FormattedMessage {...messages.startProjectHeader} />
            </H2>
            <Form>
              <label htmlFor="task">
                <Input
                  id="task"
                  type="text"
                  placeholder="Enter new task"
                  value={this.props.task}
                  onChange={this.props.onChangeTask}
                />
              </label>
            </Form>
            <Button onClick={() => this.props.onSubmitForm(this.props.task)}>
              <FormattedMessage {...messages.addButton} />
            </Button>
          </Section>
        </div>
      </article>
    );
  }
}

HomePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  repos: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  onSubmitForm: PropTypes.func,
  task: PropTypes.string,
  onChangeTask: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    onChangeTask: evt => dispatch(changeTask(evt.target.value)),
    onSubmitForm: task => dispatch(addNewTask(task)),
  };
}

const mapStateToProps = createStructuredSelector({
  task: makeSelectTask(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'home', reducer });

export default compose(
  withReducer,
  withConnect,
)(HomePage);
