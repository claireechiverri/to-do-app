/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.get('home', initialState);

const makeSelectTask = () =>
  createSelector(selectHome, homeState => homeState.get('task'));

export { selectHome, makeSelectTask };
