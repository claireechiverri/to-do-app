import { fromJS } from 'immutable';

import { selectHome, makeSelectTask } from '../selectors';

describe('selectHome', () => {
  it('should select the home state', () => {
    const homeState = fromJS({
      userData: {},
    });
    const mockedState = fromJS({
      home: homeState,
    });
    expect(selectHome(mockedState)).toEqual(homeState);
  });
});

describe('makeSelectTask', () => {
  const taskSelector = makeSelectTask();
  it('should select the task', () => {
    const task = 'test-1';
    const mockedState = fromJS({
      home: {
        task,
      },
    });
    expect(taskSelector(mockedState)).toEqual(task);
  });
});
