/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'boilerplate.components.Header';

export default defineMessages({
  title: {
    id: `${scope}.title`,
    defaultMessage: 'To-Do App!',
  },
  home: {
    id: `${scope}.add`,
    defaultMessage: 'Add New',
  },
  tasks: {
    id: `${scope}.tasks`,
    defaultMessage: 'Tasks',
  },
});
