import { CHANGE_TASK } from '../constants';

import { changeTask } from '../actions';

describe('Home Actions', () => {
  describe('changeTask', () => {
    it('should return the correct type and the passed name', () => {
      const fixture = 'Max';
      const expectedResult = {
        type: CHANGE_TASK,
        name: fixture,
      };

      expect(changeTask(fixture)).toEqual(expectedResult);
    });
  });
});
