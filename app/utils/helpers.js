const TASK_LIST = 'task_list';

export function getTasks() {
  const tasks = localStorage.getItem(TASK_LIST);
  if (tasks) {
    return JSON.parse(tasks);
  }

  localStorage.setItem(TASK_LIST, JSON.stringify([]));
  return [];
}

export function addTask(newTask) {
  let tasks = getTasks();
  if (tasks) tasks.push(newTask);
  else tasks = [newTask];
  localStorage.setItem(TASK_LIST, JSON.stringify(tasks));
}

export function removeTask(taskIndex) {
  const tasks = getTasks();
  tasks.splice(taskIndex, 1);
  localStorage.setItem(TASK_LIST, JSON.stringify(tasks));
}
