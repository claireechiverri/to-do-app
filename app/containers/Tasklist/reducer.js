/*
 * TaskList Reducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';

import { GET_TASKS_OK, REMOVE_TASK_OK } from './constants';

// The initial state of the App
export const initialState = fromJS({
  tasks: [],
});

function taskListReducer(state = initialState, action) {
  switch (action.type) {
    case GET_TASKS_OK:
    case REMOVE_TASK_OK:
      return state.set('tasks', action.tasks);
    default:
      return state;
  }
}

export default taskListReducer;
