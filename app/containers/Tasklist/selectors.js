/**
 * TaskList selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectTaskList = state => state.get('taskList', initialState);

const makeSelectTaskList = () =>
  createSelector(selectTaskList, taskListState => taskListState.get('tasks'));

export { selectTaskList, makeSelectTaskList };
