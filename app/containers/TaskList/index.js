/*
 * TaskList
 *
 * List all the tasks
 */
import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Row } from 'react-foundation-components/lib/grid-flex';

import injectReducer from 'utils/injectReducer';

import H1 from 'components/H1';
import messages from './messages';

import { getTaskList, deleteTask } from './actions';
import { makeSelectTaskList } from './selectors';
import reducer from './reducer';

import './styles.scss';

export class TaskList extends React.PureComponent {
  componentDidMount() {
    this.props.onGetTasks();
  }

  render() {
    const { tasks } = this.props;
    const listItems = [];
    tasks.forEach((task, index) => {
      listItems.push(
        <tr>
          <td>
            <span>{task}</span>
            <span>
              <button
                type="button"
                className="alert button delete-button"
                onClick={() => this.props.onRemoveTask(index)}
              >
                Delete
              </button>
            </span>
          </td>
        </tr>,
      );
    });

    return (
      <div>
        <Helmet>
          <title>Tasks Page</title>
        </Helmet>
        <H1>
          <FormattedMessage {...messages.header} />
        </H1>
        <Row>
          <table className="unstriped hover">
            <thead>
              <tr>
                <th>Name</th>
              </tr>
            </thead>

            <tbody>{listItems}</tbody>
          </table>
        </Row>
      </div>
    );
  }
}

TaskList.propTypes = {
  tasks: PropTypes.array,
  onGetTasks: PropTypes.func,
  onRemoveTask: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    onGetTasks: () => dispatch(getTaskList()),
    onRemoveTask: taskIndex => dispatch(deleteTask(taskIndex)),
  };
}

const mapStateToProps = createStructuredSelector({
  tasks: makeSelectTaskList(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'taskList', reducer });

export default compose(
  withReducer,
  withConnect,
)(TaskList);
