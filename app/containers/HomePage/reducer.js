/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';

import { ADD_TASK_OK, CHANGE_TASK } from './constants';

// The initial state of the App
export const initialState = fromJS({
  task: '',
  success: false,
});

function homeReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_TASK:
      return state.set('task', action.name);
    case ADD_TASK_OK:
      return state.set('task', '').set('success', true);
    default:
      return state;
  }
}

export default homeReducer;
