import { fromJS } from 'immutable';

import homeReducer from '../reducer';
import { addNewTask, changeTask } from '../actions';

describe('homeReducer', () => {
  let state;
  beforeEach(() => {
    state = fromJS({
      task: '',
      success: false,
    });
  });

  it('should return the initial state', () => {
    const expectedResult = state;
    expect(homeReducer(undefined, {})).toEqual(expectedResult);
  });

  it('should handle the changeTask action correctly', () => {
    const fixture = 'test-1';
    const expectedResult = state.set('task', fixture);

    expect(homeReducer(state, changeTask(fixture))).toEqual(expectedResult);
  });

  it('should handle the addNewTask action correctly', () => {
    const fixture = 'test-1';
    const expectedResult = state.set('task', '').set('success', true);

    expect(homeReducer(state, addNewTask(fixture))).toEqual(expectedResult);
  });
});
