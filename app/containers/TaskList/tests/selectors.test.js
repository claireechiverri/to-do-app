import { fromJS } from 'immutable';

import { selectTaskList } from '../selectors';

describe('selectTaskList', () => {
  it('should select the taskList state', () => {
    const taskListState = fromJS({
      data: {},
    });
    const mockedState = fromJS({
      taskList: taskListState,
    });
    expect(selectTaskList(mockedState)).toEqual(taskListState);
  });
});
