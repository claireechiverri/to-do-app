/*
 * TaskList Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import { getTasks, removeTask } from 'utils/helpers';
import { GET_TASKS_OK, REMOVE_TASK_OK } from './constants';

/**
 * Retrieves the list of tasks
 *
 * @param
 *
 * @return {array} List of tasks
 */
export function getTaskList() {
  return {
    type: GET_TASKS_OK,
    tasks: getTasks(),
  };
}

/**
 * Remove the task
 *
 * @param {task} Name of the task to be removed
 *
 * @return {}
 */
export function deleteTask(taskIndex) {
  removeTask(taskIndex);
  return {
    type: REMOVE_TASK_OK,
    tasks: getTasks(),
  };
}
