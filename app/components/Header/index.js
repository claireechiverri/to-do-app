import React from 'react';
import { FormattedMessage } from 'react-intl';

import H1 from 'components/H1';
import NavBar from './NavBar';
import HeaderLink from './HeaderLink';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
class Header extends React.Component {
  render() {
    return (
      <div>
        <H1>
          <FormattedMessage {...messages.title} />
        </H1>
        <NavBar>
          <HeaderLink to="/">
            <FormattedMessage {...messages.tasks} />
          </HeaderLink>
          <HeaderLink to="/add">
            <FormattedMessage {...messages.home} />
          </HeaderLink>
        </NavBar>
      </div>
    );
  }
}

export default Header;
