import { fromJS } from 'immutable';

import taskListReducer from '../reducer';
import { getTaskList } from '../actions';

describe('taskListReducer', () => {
  let state;
  beforeEach(() => {
    state = fromJS({
      tasks: [],
    });
  });

  it('should return the initial state', () => {
    const expectedResult = state;
    expect(taskListReducer(undefined, {})).toEqual(expectedResult);
  });

  it('should handle the getTaskList action correctly', () => {
    const fixture = [];
    const expectedResult = state.set('tasks', fixture);

    expect(taskListReducer(state, getTaskList())).toEqual(expectedResult);
  });
});
