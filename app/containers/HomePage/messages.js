/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'boilerplate.containers.HomePage';

export default defineMessages({
  addButton: {
    id: `${scope}.button.add`,
    defaultMessage: 'Add',
  },
  startProjectHeader: {
    id: `${scope}.start_project.header`,
    defaultMessage: 'Add New To-Do',
  },
});
