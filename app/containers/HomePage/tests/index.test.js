/**
 * Test the HomePage
 */

import { mapDispatchToProps } from '../index';
import { addNewTask, changeTask } from '../actions';

describe('<HomePage />', () => {
  describe('mapDispatchToProps', () => {
    describe('onChangeTask', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.onChangeTask).toBeDefined();
      });

      it('should dispatch changeTask when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const task = 'test-1';
        result.onChangeTask({ target: { value: task } });
        expect(dispatch).toHaveBeenCalledWith(changeTask(task));
      });
    });

    describe('onSubmitForm', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.onSubmitForm).toBeDefined();
      });

      it('should dispatch addNewTask when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const task = 'test-1';
        result.onSubmitForm(task);
        expect(dispatch).toHaveBeenCalledWith(addNewTask(task));
      });
    });
  });
});
