/**
 * Test the TaskList
 */

import { mapDispatchToProps } from '../index';
import { getTaskList, deleteTask } from '../actions';

describe('<TaskList />', () => {
  describe('mapDispatchToProps', () => {
    describe('onGetTasks', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.onGetTasks).toBeDefined();
      });

      it('should dispatch changeTask when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.onGetTasks();
        expect(dispatch).toHaveBeenCalledWith(getTaskList());
      });
    });
    describe('onRemoveTask', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.onRemoveTask).toBeDefined();
      });

      it('should dispatch removeTask when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const task = 'test-1';
        result.onRemoveTask(task);
        expect(dispatch).toHaveBeenCalledWith(deleteTask(task));
      });
    });
  });
});
